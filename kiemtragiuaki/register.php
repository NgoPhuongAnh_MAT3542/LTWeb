<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Hiển thị thông tin đăng ký sinh viên</title>
</head>

<body>
    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $username = $_POST["username"];
        $gender = $_POST["gender"];
        $dob = $_POST["year"] . "-" . $_POST["month"] . "-" . $_POST["day"];
        $city = $_POST["city"];
        $district = $_POST["district"];
        $email = $_POST["email"];
        $phone = $_POST["phone"];
        $university = $_POST["university"];
        $major = $_POST["major"];

        echo "<h2>Thông tin đăng ký sinh viên</h2>";
        echo "<p><strong>Họ và tên:</strong> " . $username . "</p>";
        echo "<p><strong>Giới tính:</strong> " . $gender . "</p>";
        echo "<p><strong>Ngày sinh:</strong> " . $dob . "</p>";
        echo "<p><strong>Địa chỉ:</strong> " . $district . ", " . $city . "</p>";
    }
    ?>
</body>

</html>
