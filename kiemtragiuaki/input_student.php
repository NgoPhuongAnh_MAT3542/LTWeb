<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form đăng ký sinh viên</title>
    <style>
        .outer-box {
            padding: 50px;
            width: 500px;
            margin: 0 auto;
            background-color: #f2f2f2;
            border-radius: 5px;
        }

        h2 {
            text-align: center;
            margin-bottom: 30px;
        }

        .register-form {
            margin-bottom: 20px;
        }

        .register-form-item {
            display: flex;
            align-items: center;
            margin-bottom: 15px;
        }

        label {
            flex-basis: 30%;
            margin-right: 10px;
            text-align: right;
            font-weight: bold;
        }

        .required {
            color: red;
            margin-left: 5px;
        }

        input[type="text"],
        select,
        textarea {
            flex-grow: 1;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 5px;
        }

        select {
            appearance: none;
            background-color: white;
            background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 24 24' fill='%23333333' width='18px' height='18px'%3E%3Cpath d='M7 10l5 5 5-5z'/%3E%3C/svg%3E");
            background-repeat: no-repeat;
            background-position: right 10px center;
            background-size: 12px;
            padding-right: 30px;
        }

        button[type="submit"] {
            display: block;
            margin: 0 auto;
            padding: 10px 20px;
            background-color: #4caf50;
            color: white;
            border: none;
            border-radius: 5px;
            cursor: pointer;
            font-size: 16px;
            font-weight: bold;
            transition: background-color 0.3s ease;
        }

        button[type="submit"]:hover {
            background-color: #45a049;
        }

        #error-message {
            color: red;
            margin-top: 10px;
        }
    </style>
</head>


<body>
    <div class="outer-box">
        <form method="POST" onsubmit="return validateForm()">
            <div class="register-form">
                <div id="error-message"></div>
            </div>

            <div class="register-form">
                <label for="username">Họ và tên<span class="required">*</span></label>
                <input type="text" id="username" name="username" size="32" required>
            </div>

            <div class="register-form">
                <label class="gender">Giới tính<span class="required">*</span></label>
                <input type="radio" id="gender_0" name="gender" value="Nam" required>
                <label for="gender_0">Nam</label>
                <input type="radio" id="gender_1" name="gender" value="Nữ" required>
                <label for="gender_1">Nữ</label>
            </div>

            <div class="register-form">
                <label class="register-form-item" for="dob">Ngày sinh<span class="required">*</span></label>
                <select id="year" name="year" required>
                    <option value="">Năm</option>
                </select>
                <select id="month" name="month" required>
                    <option value="">Tháng</option>
                </select>
                <select id="day" name="day" required>
                    <option value="">Ngày</option>
                </select>
            </div>

            <div class="register-form">
            	<label class="register-form-item">Địa chỉ<span class="required">*</span></label>
                <label class="register-form-item" for="city">Thành phố<span class="required">*</span></label>
                <select id="city" name="city" onchange="loadDistricts()" required>
                    <option value="">--Chọn thành phố--</option>
                    <option value="Hanoi">Hà Nội</option>
                    <option value="HoChiMinh">Hồ Chí Minh</option>
                </select>
            </div>

            <div class="register-form">
                <label class="register-form-item" for="district">Quận<span class="required">*</span></label>
                <select id="district" name="district" required></select>
            </div>


            <div class="register-form">
                <div class="additional-info">
                    <label for="additional-info">Thông tin bổ sung</label>
                    <textarea id="additional-info" name="additional-info" rows="4" cols="30"></textarea>
                </div>
            </div>

            <div class="register-form">
                <input type="submit" value="Gửi đăng ký">
            </div>
        </form>
    </div>

        <script>
        function validateForm() {
            var username = document.getElementById('username').value;
            var dob = document.getElementById('dob').value;
            var city = document.getElementById('city').value;
            var district = document.getElementById('district').value;

            var errorMessage = '';

            if (username.trim() === '') {
                errorMessage += 'Họ và tên không được để trống.\n';
            }

            if (dob.trim() === '') {
                errorMessage += 'Ngày sinh không được để trống.\n';
            }

            if (city.trim() === '') {
                errorMessage += 'Thành phố không được để trống.\n';
            }

            if (district.trim() === '') {
                errorMessage += 'Quận không được để trống.\n';
            }

            if (errorMessage !== '') {
                document.getElementById('error-message').innerText = errorMessage;
                return false;
            }

            return true;
        }


        function loadDistricts() {
            var city = document.getElementById("city").value;
            var districtSelect = document.getElementById("district");

            // Clear existing options
            districtSelect.innerHTML = "";

            if (city === "Hanoi") {
                var districts = ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"];

            } else if (city === "HoChiMinh") {
                var districts = ["Quận 1", "Quận 2", "Quận 3", "Quận 7", "Quận 9"];
            }

            // Add options for districts
            for (var i = 0; i < districts.length; i++) {
                var option = document.createElement("option");
                option.text = districts[i];
                option.value = districts[i];
                districtSelect.add(option);
            }
        }

        // Thêm tùy chọn năm
        var yearSelect = document.getElementById("year");
        var currentYear = new Date().getFullYear();
        for (var year = 1983; year <= currentYear; year++) {
            var option = document.createElement("option");
            option.text = year;
            option.value = year;
            yearSelect.add(option);
        }

        // Thêm tùy chọn tháng
        var monthSelect = document.getElementById("month");
        for (var month = 1; month <= 12; month++) {
            var option = document.createElement("option");
            option.text = month;
            option.value = month;
            monthSelect.add(option);
        }

        // Thêm tùy chọn ngày
        var daySelect = document.getElementById("day");
        for (var day = 1; day <= 31; day++) {
            var option = document.createElement("option");
            var paddedDay = day.toString().padStart(2, "0");
            option.text = paddedDay;
            option.value = paddedDay;
            daySelect.add(option);
        }
    </script>
</body>
</html>

