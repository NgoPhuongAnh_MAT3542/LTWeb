<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Register Form</title>
	<style>
        body {
            font-family: 'Times New Roman', Times, serif;
        }

        .outer-box {
            padding: 60px;
            width: 40%;
            border: 2px solid #648bae;
            margin: 10% auto 0 auto;
            display: flex; /* Thêm thuộc tính display:flex để các ô bên trong outer-box được xếp hàng ngang */
            justify-content: space-between; /* Thêm thuộc tính justify-content: space-between để các ô cột được căn đều trên hàng ngang */
        }

        .register-form {
            width: 80%;
            margin-left: auto;
            margin-right: auto;
        }

        .register-form {
            margin-top: 10px;
            display: flex;
            align-items: center;
            justify-content: space-between;
        }

        .left {
            flex: 0.3;
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
            margin-right: 20px; 
            margin-left: -55px; 
        }

        label {
            margin-bottom: 10px;
            padding: 10px;
            box-sizing: border-box;
            color: white;
            background-color: #5b9bd5;
            border: 2px solid #648bae;
            margin-right: 0; 
            font-size: 16px; 
            text-align: center;
        }

        .right {
            flex: 0.7; 
            box-sizing: border-box;
            display: flex;
            flex-direction: column;
        }

        input[type="text"] {
            width: 130%;
            padding: 10px;
            border: 1px solid #ccc;
            margin-bottom: 20px;
            box-sizing: border-box;
            border: 2px solid #648bae;
            margin-right: 80px;
        }   

        select {
            width: 80%;
            padding: 10px;
            border: 1px solid #ccc;
            margin-bottom: -10px;
            margin-top:10px
            box-sizing: border-box;
            border: 2px solid #648bae;
            background-color: #FFFFFF;
        }
                

        .radio-group {
            display: flex;
            gap: 10px;
            margin-bottom: 20px;
            text-align: center;
            
        }

        button {
            margin: 5% auto 0 auto;
            display: block;
            font-family: inherit;
            width: 30%;
            padding: 10px;
            border: 2px solid #648bae;
            background-color: #99CC00;
            color: white;
            border-radius: 10px;
            font-size: 1rem;
        }
    </style>
</head>
<body>
    <div class="outer-box">
        <form method="POST">

            <div class="register-form">
                <div class="left">
                    <label for="username">Họ và tên</label>
                    <label for="gender">Giới tính</label>
                    <label for="major">Phân khoa</label>
                </div>
                <div class="right">
                    <input type="text" id="username" name="username" size ="32">
                
                    <div class="radio-group">
                        <input type="radio" id="gender_male" name="gender" value="0"> Nam
                        <input type="radio" id="gender_female" name="gender" value="1">Nữ
                        <?php
                        $genders = array("0" => "Nam", "1" => "Nữ");

                        for ($genders; $key; $value) {
                            echo "<input type='radio' id='gender_$key' name='gender' value='$key'> ";
                            echo "<label for='gender_$key'>$value</label>";
                        }
                        ?>
                    </div>

                    <select id="majorelect" name="major">
                        <option value="--Chọn phân khoa--">--Chọn phân khoa--</option>
                        <?php
                        $majors = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");

                        foreach ($majors as $key => $value) {
                            echo "<option value='$key'>$value</option>";
                        }
                        ?>
                    </select><br>

                </div>
            </div>
            <button>Đăng ký</button>
        </form>
    </div>

</body>
</html>