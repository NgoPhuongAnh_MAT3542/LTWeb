CREATE TABLE `SinhVien` (
  `MaSV` varchar(6) NOT NULL,
  `HoSV` varchar(30) DEFAULT NULL,
  `TenSV` varchar(15) DEFAULT NULL,
  `GioiTinh` char(1) DEFAULT NULL,
  `NgaySinh` datetime DEFAULT NULL,
  `NoiSinh` varchar(50) DEFAULT NULL,
  `DiaChi` varchar(50) DEFAULT NULL,
  `MaKH` varchar(6) DEFAULT NULL,
  `HocBong` int DEFAULT NULL,
  PRIMARY KEY (`MaSV`)
)

CREATE TABLE `DMKHOA` (
  `MaKH` varchar(6) NOT NULL,
  `TenKhoa` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`MaKH`)
)

SELECT * FROM sinhvien s
JOIN dmkhoa d on s.MaKH = d.MaKH
WHERE d.TenKhoa = "Công nghệ thông tin"
